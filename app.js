var express = require('express')
    , path = require('path')
    , fs = require('fs')
    , glob = require('glob')
    , favicon = require('serve-favicon')
    , logger = require('morgan')
    , bodyParser = require('body-parser')
    , session = require('express-session')
    , cookieParser = require('cookie-parser')
    , cookieSession = require('cookie-session')
    , mongoose = require('mongoose')
    , app = express()
    , MongoStore = require('connect-mongo')(session)
    , useragent = require('express-useragent')
    , hbs = require('express-hbs');

//DEBUG VARIABLE

var DEBUG = true;

// DEBUG VARIABLE
mongoose.connect('mongodb://localhost/DatabaseName');
mongoose.set('debug', DEBUG);

//helmet handles some cool security shit
app.use(require('helmet')());

/*
    Usage: 
    {{#xif var1 '==' var2}}
        True
    {{/xif}}

    == can be any of the other operators (>, <, <=, >=, !==, ===, &&, ||)
*/
hbs.registerHelper('xif', function (v1, operator, v2, options) {

    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '!=':
            return (v1 != v2) ? options.fn(this) : options.inverse(this);
        case '!==':
            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});

//Setup handlebars
app.engine('hbs', hbs.express4({
    partialsDir: __dirname + '/views',
    defaultLayout: __dirname + "/views/layout",
    beautify: true
}));

var blocks = {};

hbs.registerHelper('extend', function(name, context) {
    var block = blocks[name];
    if (!block) {
        block = blocks[name] = [];
    }

    block.push(context.fn(this));
});

hbs.registerHelper('block', function(name) {
    var val = (blocks[name] || []).join('\n');

    // clear the block
    blocks[name] = [];
    return val;
});

app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

app.set('trust proxy', 1);

//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(useragent.express());
app.use(cookieParser("CookieSecretHereWoopWoooop"));
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: 'SessionSecretHereWoopWoooop',
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    resave: false,
    saveUninitialized: true
}));

app.use(function(req, res, next){
    //This function here is called everytime a user loads any page! a good place to check if user is logged in and such.
    req.isMobile = function() { return req.useragent.isMobile; }
    
    // pass the mobile variable to views, used in layout.hbs to check if user is on mobile or not.
    res.locals.mobile = req.useragent.isMobile;

    next(); //neeext
});

//call all files inside routes folder, so you don't have to call each one alone...
glob.sync( './routes/*.js' ).forEach( function( file ) {
    var router = require( path.resolve(file));
    app.use(require( path.resolve(file)));
});

app.use(function (req, res, next) {
    // this one here is the last handler that is called, if all the handlers didn't match the user's request.
    // You want to leave this one at the very end and handle 404 error page here
    res.render("404");
});

app.listen(80, function(){
    console.log('Web server started on port 80.');
});

module.exports = app;